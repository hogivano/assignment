# Gfinity Front-end Assignment
Have are 3 main Page.
1. Lobby
2. Searching
3. Found

Timer work start at 01:00

Live demo : [demo](https://gfinity-d3120.firebaseapp.com)
## Build Setup

```bash
# install dependencies
$ npm run install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).
