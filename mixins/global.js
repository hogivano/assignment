export default {
  methods: {
    findList(arr, id) {
      const filter = arr.filter(val => val.id === id)

      if (filter.length > 0) {
        return filter[0].name
      }

      return ''
    }
  }
}
